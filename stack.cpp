#include <stdio.h>
#include <stdlib.h>

struct ListNode
{
	int value;
	struct ListNode* prev;
	struct ListNode* next;
};

struct Stack
{
	struct ListNode* head;
};

struct ListNode* addBefore(struct ListNode* node, int value)
{
	if(node == NULL)
	{
		return;
	}
	
	struct ListNode* prevNode = node->prev;
	
	struct ListNode* newNode = (struct ListNode*)malloc(sizeof(struct ListNode));
	
	newNode->value = value;
	newNode->next = node;
	
	node->prev = newNode;
	
	if(prevNode != NULL)
	{
		prevNode->next = newNode;
		newNode->prev = prevNode;
	}
	else
	{
		newNode->prev = NULL;
	}
	
	return newNode;
}

struct ListNode* addAfter(struct ListNode* node, int value)
{
	if(node == NULL)
	{
		return;
	}
	
	struct ListNode* nextNode = node->next;
	
	struct ListNode* newNode = (struct ListNode*)malloc(sizeof(struct ListNode));
	
	newNode->value = value;
	newNode->prev = node;
	
	node->next = newNode;
	
	if(nextNode != NULL)
	{
		nextNode->prev = newNode;
		newNode->next = nextNode;
	}
	else
	{
		newNode->next = NULL;
	}
	
	return newNode;	
}

void remove(struct ListNode* node)
{
	if(node == NULL)
	{
		return;
	}
	
	struct ListNode* prevNode = node->prev;
	struct ListNode* nextNode = node->next;
	
	free(node);
	
	if(prevNode != NULL)
	{
		prevNode->next = nextNode;
	}
	
	if(nextNode != NULL)
	{
		nextNode->prev = prevNode;
	}
}

struct Stack* create();
{
	struct Stack* s = (struct Stack*)malloc(sizeof(struct Stack));
	s->head = NULL;
	return s;
}

void addToStack(struct Stack* stack, int value)
{
	if(stack == NULL)
	{
		return;
	}
	
	if(stack->head == NULL)
	{
		stack->head = (struct ListNode*)malloc(sizeof(struct ListNode));
		stack->head->value = value;
		stack->head->next = NULL;
		stack->head->prev = NULL;
	}
	else
	{
		stack->head = addBefore(stack->head, value);
	}
}

int top(struct Stack* stack)
{
	if(stack == NULL || stack->head == NULL)
	{
		return 0;
	}
	
	return stack->head->value;
}

void removeFromStack(struct Stack* stack)
{
	if(stack == NULL || stack->head == NULL)
	{
		return;
	}
	
	struct ListNode* removedNode = stack->head;
	struct ListNode* newHead = removedNode->next;
	remove(removedNode);
	stack->head = newHead;
}

void main(int argc, char *argv[])
{
	struct ListNode* head = (struct ListNode*)malloc(sizeof(struct ListNode));
	struct ListNode* tail = head;
	head->value = 0;
	head->next = NULL;
	head->prev = NULL;
	
	head = addBefore(head, 1);
	addAfter(head, 2);
	addBefore(tail, 4);
	tail = addAfter(tail, 3);
	
	struct Stack* stack = create();
	addToStack(stack, 2);
	addToStack(stack, 7);
	printf("%i\n", top(stack));
	removeFromStack(stack);
	printf("%i\n", top(stack));
	removeFromStack(stack);
}

